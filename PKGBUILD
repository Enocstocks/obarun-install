# Copyright (c) 2015-2021 Eric Vidal <eric@obarun.org>
# All rights reserved.
#
# This file is part of Obarun. It is subject to the license terms in
# the LICENSE file found in the top-level directory of this
# distribution.
# This file may not be copied, modified, propagated, or distributed
# except according to the terms contained in the LICENSE file.
#
# Maintainer: Obarun-install scripts <eric@obarun.org>
# DO NOT EDIT this PKGBUILD if you don't know what you do

pkgname=obarun-install

pkgdesc="Script for automatic installation"

pkgver=23536df
pkgrel=1

url="file:///var/lib/obarun/$pkgname/update_package/$pkgname"

source=("$pkgname::git+file:///var/lib/obarun/$pkgname/update_package/$pkgname")

#--------------------------------------| BUILD PREPARATION |------------------------------------

pkgver() {
    cd "${pkgname}"

    git describe --tags | sed -e 's:-:+:g;s:^v::'
}

#-------------------------------------------| PACKAGE |-----------------------------------------

package() {
    cd "${pkgname}"

    make DESTDIR="$pkgdir" install
}

#------------------------------------| INSTALL CONFIGURATION |----------------------------------

arch=(x86_64)

backup=('etc/obarun/install.conf')

depends=(
    'arch-install-scripts'
    'expac'
    'rsync'
    'mc'
    'git'
    'pacman'
    'pacman-contrib'
    'cower'
    'obarun-libs>=0.2.1'
    'obarun-install-themes'
    'dialog'
    'parted'
    'gptfdisk'
    'oblog')

#-------------------------------------| SECURITY AND LICENCE |----------------------------------

md5sums=('SKIP')
license=(ISC)
