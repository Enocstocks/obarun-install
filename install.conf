# Copyright (c) 2015-2021 Eric Vidal <eric@obarun.org>
# All rights reserved.
#
# This file is part of Obarun. It is subject to the license terms in
# the LICENSE file found in the top-level directory of this
# distribution.
# This file may not be copied, modified, propagated, or distributed
# except according to the terms contained in the LICENSE file.

##---------------------------------------------------------------------#
## Fresh installation configuration
##---------------------------------------------------------------------#

# define name of the host for the new installation
HOSTNAME="Ob66"

# define the locale for the new installation
LOCALE="en_US.UTF-8"

# define the zone for the new installation e.g Europe
ZONE="GMT"

# define the subzone for the new installation e.g. Londres
SUBZONE=""

# define the keymap for the console for the new installation
KEYMAP="us"

# define the keymap for the Desktop for the new installation
XKEYMAP="us"

# define the name for a new user for the new installation
NEWUSER="oblive"

##---------------------------------------------------------------------#
## Installer configuration
##---------------------------------------------------------------------#

# editor used during the installation
EDITOR="mcedit"

# valid mountpoint for installation
NEWROOT="/mnt"

# configuration files directory used for the installation
CONFIG_DIR="minimal"

# cache directory used during the installation
CACHE_DIR="/var/cache/pacman/pkg"

# use rankmirrors to find the fastest local mirror
# expected value are yes or no without uppercase
RANKMIRRORS="no"

# firmware interface to use for the installation
FIRMWARE="BIOS"

# check network at the start of the script
NETWORKCHECK="yes"

# check the keyboard layout
KEYBOARDCHECK="yes"

# scheme color configuration file
SCHEMECOLOR="/etc/obarun/dialog.conf"

# bootloader by default
# field accepted: Sylinux,GRUB,EFISTUB
BOOTLOADER="Syslinux"

##---------------------------------------------------------------------#
## Installer update configuration
##---------------------------------------------------------------------#

# allow to update the obarun-install script when it launched [yes|no]
UPDATE_INS="yes"

# allow to update the obarun-install-themes script when it launched [yes|no]
UPDATE_TPL="yes"

# Be more/less verbose
# 1 for more, 0 for less
DEBUG=0

# git source of obarun-install package
GIT_ADDR="https://framagit.org/Obarun/obarun-install.git"

# git source of obarun-install-themes package
GIT_ADDR_THEMES="https://framagit.org/Obarun/obarun-install-themes.git"

# git branch to use for obarun-install, if not set master is took by default
BRANCH=""

# git branch to use for obarun-install-themes, if not set master is took by default
BRANCH_THEMES=""

# gpg directory used during the installation
GPG_DIR="/var/lib/obarun/gnupg"
